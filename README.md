----- WEB SEMANTIQUE -----

Sujet : "Pharmaceutical drug spending by countries"




Les fichiers utilisés/créés sont classés par étape.

Les fichiers ttl des liaisons ont été créés en regroupant notre fichier ttl ainsi que celui du groupe de l'autre liaison.

Dans certains de ces fichiers, il a été nécessaire d'ajouter un fichier ttl d'un autre dataset pour notamment lier les pays, notre fichier contenant les CountryCode tandis que d'autres avaient seulement les pays.




 - Etape 1 :
 
    construct.sparql
    fichier.ttl
    requete1.rq
    requete2.rq

 - Etape 2 :

    fichierL1.ttl
    RequeteL1.rq
    
    fichierLiaison2.ttl
    RequeteLiaison2.rq
    
    fichierL3.ttl
    RequeteL3.rq
    
    fichierLiaison4.ttl
    RequeteLiaison4.rq
    
    fichierL5.ttl
    RequeteL5.rq
    
    fichierL6.ttl
    RequeteL6.rq
    
    fichierL7.ttl
    RequeteL7.rq
    
    fichierL8.ttl
    RequeteL8.rq

 - Etape 3 :

    Medicament.owl
    
 - Etape 5 :

    VOID.txt